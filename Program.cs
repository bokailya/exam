﻿using System;
using System.Linq;

namespace exam
{
    class GaussianElumination
    {
        class SystemOfLinearEquationsWithNonSingularMatrix
        {
            private class InvalidInputException : Exception
            {
                public InvalidInputException(string message)
                    : base(message)
                {
                }
            }

            public SystemOfLinearEquationsWithNonSingularMatrix()
            {
                string[] inputLines
                    = System.IO.File.ReadLines("input").ToArray();
                size = (uint)inputLines.Length;
                if (size == 0)
                    throw new InvalidInputException("Empty input.");
                coefficientsMatrix = new double[size, size];
                rightSide = new double[size];
                foreach (
                        var it1
                        in
                        inputLines.Select((Line, Index) => new {Line, Index}))
                {
                    string[] elements
                        = it1.Line.Split(
                                (char [])null,
                                StringSplitOptions.RemoveEmptyEntries)
                        .ToArray();
                    if (elements.Length != size + 1)
                        throw
                            new
                            InvalidInputException(
                                    "Invalid row "
                                    + (it1.Index + 1)
                                    + " length: Expected "
                                    + (size + 1)
                                    + " found "
                                    + elements.Length
                                    + '.');
                    foreach (
                            var it2
                            in
                            it1.Line.Split()
                            .Select(
                                (element, Index)
                                =>
                                new {Element = double.Parse(element), Index}))
                        if (it2.Index < size)
                            coefficientsMatrix[it1.Index, it2.Index]
                                = it2.Element;
                        else
                            rightSide[it1.Index] = it2.Element;
                }
            }


            // Gaussian Elimination
            public double[] Solve()
            {
                void Swap(ref uint lhs, ref uint rhs)
                {
                    uint tmp = lhs;
                    lhs = rhs;
                    rhs = tmp;
                }


                uint[] row_permutation = new uint[size];
                for (uint row = 0; row < size; ++row) 
                    row_permutation[row] = row;

                // Elimination
                for (uint i = 0; i < size - 1; ++i) 
                {
                    uint pivot = i;
                    double maxAbs = 0;
                    for (uint row = i + 1; row < size; ++row)
                    {
                        if (
                            Math.Abs(
                                coefficientsMatrix[row_permutation[row], i])
                            > maxAbs)
                        {
                            pivot = row;
                            maxAbs
                            = Math.Abs(
                                coefficientsMatrix[row_permutation[row], i]);
                        }
                    }
                    Swap(
                        ref row_permutation[i],
                        ref row_permutation[pivot]);
                    for (uint row = i + 1; row < size; ++row)
                    {
                        double multipl
                        = -coefficientsMatrix[row_permutation[row], i]
                        / coefficientsMatrix[row_permutation[i], i];
                        for (uint j = i; j < size; ++j)
                            coefficientsMatrix[row_permutation[row], j]
                            += coefficientsMatrix[
                                row_permutation[i], j]
                                * multipl;
                        rightSide[row_permutation[row]]
                        += rightSide[row_permutation[i]] * multipl;
                    }
                }

                // Back substituion
                double[] answer = new double[size];
                for (int variable = (int)(size - 1); variable >= 0; --variable)
                {
                    answer[variable] = rightSide[row_permutation[variable]];
                    for (uint j = (uint)(variable + 1); j < size; ++j)
                        answer[variable]
                            -=
                            answer[j]
                            * coefficientsMatrix[row_permutation[variable], j];
                    answer[variable]
                        /=
                        coefficientsMatrix[row_permutation[variable], variable];
                }

                return answer;
            }


            private double[,] coefficientsMatrix;
            private double[] rightSide;
            private uint size;
        }

        private enum ExitCode {OK, Error};

        private static int Main(string[] args)
        {
            const string ProgramName = "exam";

            try
            {
                Console.WriteLine(
                        "Result: "
                        +
                        string.Join(
                            ' ',
                            new
                            SystemOfLinearEquationsWithNonSingularMatrix()
                            .Solve()));
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(ProgramName + ": " + exception.Message);
                return (int)ExitCode.Error;
            }
            return (int)ExitCode.OK;
        }
    }
}
